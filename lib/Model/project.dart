final String tableProject = "tblProjects";

class ProjectFields {
  static final List<String> values = [
    projectId,clientId,projectName,date
  ];

  static final String projectId = "projectId";
  static final String clientId = "clientId";
  static final String projectName = "projectName";
  static final String date = "date";
}

class Project {
  int? projectId;
  int? clientId;
  String? projectName;
  String? date;

  Project(this.clientId,this.projectName,this.date);

  Project.withDetails(this.projectId,this.clientId,this.projectName,this.date);

  int? get getProjectId => projectId;
  int? get getClientId => clientId;
  String? get getProjectName => projectName;
  String? get getDate => date;

  set setProjectId(int? projectId) {
    this.projectId = projectId;
  }

  set setClientId(int? clientId) {
    this.clientId = clientId;
  }

  set setProjectName(String? projectName) {
    this.projectName = projectName;
  }

  set setDate(String? date) {
    this.date = date;
  }

  Map<String, Object?> toMap() => {
    ProjectFields.projectId : projectId,
    ProjectFields.clientId : clientId,
    ProjectFields.projectName : projectName,
    ProjectFields.date : date
  };

  Project copy({
    int? projectId,
    int? clientId,
    String? projectName,
    String? date
  }) => Project.withDetails(projectId, clientId, projectName, date);

  static Project fromJson(Map<String,Object?> json) => Project.withDetails(
      json[ProjectFields.projectId] as int,
      json[ProjectFields.clientId] as int,
      json[ProjectFields.projectName] as String,
      json[ProjectFields.date] as String);
}