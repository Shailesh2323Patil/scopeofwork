final String tableDocumentVersion = "tblDocumentVersion";

class DocumentVersionFields {
  static final List<String> values = [
    documentVersionId,documentVersionName,projectId,documentTypeId,createdDate,updatedDate
  ];

  static final String documentVersionId = "documentVersionId";
  static final String documentVersionName = "documentVersionName";
  static final String projectId = "projectId";
  static final String documentTypeId = "documentTypeId";
  static final String createdDate = "createdDate";
  static final String updatedDate = "updatedDate";
}

class DocumentVersion {
  int? documentVersionId;
  String? documentVersionName;
  int? projectId;
  int? documentTypeId;
  String? createdDate;
  String? updatedDate;

  DocumentVersion(this.documentVersionName,this.projectId,this.documentTypeId,this.createdDate,this.updatedDate);

  DocumentVersion.withDetails(this.documentVersionId,this.documentVersionName,this.projectId,this.documentTypeId,this.createdDate,this.updatedDate);

  int? get getDocumentVersionId => documentVersionId;

  String? get getDocumentVersionName => documentVersionName;

  int? get getProjectId => projectId;

  int? get getDocumentTypeId => documentTypeId;

  String? get getCreatedDate => createdDate;

  String? get getUpdatedDate => updatedDate;

  set setDocumentVersionId(int? documentVersionId) {
    this.documentVersionId = documentVersionId;
  }

  set setDocumentVersionName(String? documentVersionName) {
    this.documentVersionName = documentVersionName;
  }

  set setProjectId(int? projectId) {
    this.projectId = projectId;
  }

  set setDocumentTypeId(int? documentTypeId) {
    this.documentTypeId = documentTypeId;
  }

  set setCreatedDate(String? createdDate) {
    this.createdDate = createdDate;
  }

  set setUpdatedDate(String? updatedDate) {
    this.updatedDate = updatedDate;
  }

  Map<String, Object?> toMap() => {
    DocumentVersionFields.documentVersionId : documentVersionId,
    DocumentVersionFields.documentVersionName : documentVersionName,
    DocumentVersionFields.projectId : projectId,
    DocumentVersionFields.documentTypeId : documentTypeId,
    DocumentVersionFields.createdDate : createdDate,
    DocumentVersionFields.updatedDate : updatedDate
  };

  DocumentVersion copy({
    int? documentVersionId,
    String? documentVersionName,
    int? projectId,
    int? documentTypeId,
    String? createdDate,
    String? updatedDate
  }) => DocumentVersion.withDetails(documentVersionId, documentVersionName, projectId, documentTypeId, createdDate, updatedDate);

  static DocumentVersion fromJson(Map<String,Object?> json) => DocumentVersion.withDetails(
    json[DocumentVersionFields.documentVersionId] as int,
    json[DocumentVersionFields.documentVersionName] as String,
    json[DocumentVersionFields.projectId] as int,
    json[DocumentVersionFields.documentTypeId] as int,
    json[DocumentVersionFields.createdDate] as String,
    json[DocumentVersionFields.updatedDate] as String);
}