final String tableDocumentDetails = "tblDocumentDetails";

class DocumentDetailsFields {
    static final List<String> values = [
        docDetailsId,documentVersionId,topicId,titleId,descriptionId,displayOrder
    ];

    static final docDetailsId = "docDetailsId";
    static final documentVersionId = "documentVersionId";
    static final topicId = "topicId";
    static final topicName = "topicName";
    static final titleId = "titleId";
    static final titleName = "titleName";
    static final descriptionId = "descriptionId";
    static final descriptionName = "descriptionName";
    static final displayOrder = "displayOrder";
}

class DocumentDetails {
    int? docDetailsId;
    int? documentVersionId;
    int? topicId;
    String? topicName;
    int? titleId;
    String? titleName;
    int? descriptionId;
    String? descriptionName;
    int? displayOrder;

    int? get getDocDetailsId => docDetailsId;

    int? get getDocumentVersionId => documentVersionId;

    int? get getTopicId => topicId;

    String? get getTopicName => topicName;

    int? get getTitleId => titleId;

    String? get getTitleName => titleName;

    int? get getDescriptionId => descriptionId;

    String? get getDescriptionName => descriptionName;

    int? get getDisplayOrder => displayOrder;

    set setDocDetailsId(int? docDetailsId) {
        this.docDetailsId = docDetailsId;
    }

    set setDocumentVersionId(int? documentVersionId) {
        this.documentVersionId = documentVersionId;
    }

    set setTopicId(int? topicId) {
        this.topicId = topicId;
    }

    set setTopicName(String? topicName) {
        this.topicName = topicName;
    }

    set setTitleId(int? titleId) {
        this.titleId = titleId;
    }

    set setTitleName(String? titleName) {
        this.titleName = titleName;
    }

    set setDescriptionId(int? descriptionId) {
        this.descriptionId = descriptionId;
    }

    set setDescriptionName(String? descriptionName) {
        this.descriptionName = descriptionName;
    }

    set setDisplayOrder(int? displayOrder) {
        this.displayOrder = displayOrder;
    }

    DocumentDetails(this.documentVersionId, this.topicId, this.titleId, this.descriptionId, this.displayOrder);

    DocumentDetails.withDetails(this.docDetailsId, this.documentVersionId, this.topicId, this.titleId, this.descriptionId, this.displayOrder);

    DocumentDetails.withDisplayOrder(this.displayOrder, this.docDetailsId);

    DocumentDetails.withFullDetails(this.docDetailsId, this.documentVersionId, this.topicId, this.topicName, this.titleId, this.titleName, this.descriptionId, this.descriptionName, this.displayOrder);

    Map<String,Object?> toMap() => {
        DocumentDetailsFields.docDetailsId : docDetailsId,
        DocumentDetailsFields.documentVersionId : documentVersionId,
        DocumentDetailsFields.topicId : topicId,
        DocumentDetailsFields.titleId : titleId,
        DocumentDetailsFields.descriptionId : descriptionId,
        DocumentDetailsFields.displayOrder : displayOrder
    };

    DocumentDetails copy({
        int? docDetailsId,
        int? documentVersionId,
        int? topicId,
        int? titleId,
        int? descriptionId,
        int? displayOrder
    }) => DocumentDetails.withDetails(docDetailsId, documentVersionId, topicId, titleId, descriptionId, displayOrder);

    static DocumentDetails fromJson(Map<String,Object?> json) => DocumentDetails.withDetails(
        json[DocumentDetailsFields.docDetailsId] as int,
        json[DocumentDetailsFields.documentVersionId] as int,
        json[DocumentDetailsFields.topicId] as int,
        json[DocumentDetailsFields.titleId] as int,
        json[DocumentDetailsFields.descriptionId] as int,
        json[DocumentDetailsFields.displayOrder] as int);

    static DocumentDetails fromFullJson(Map<String,Object?> json) => DocumentDetails.withFullDetails(
        json[DocumentDetailsFields.docDetailsId] as int?,
        json[DocumentDetailsFields.documentVersionId] as int?,
        json[DocumentDetailsFields.topicId] as int?,
        json[DocumentDetailsFields.topicName] as String?,
        json[DocumentDetailsFields.titleId] as int?,
        json[DocumentDetailsFields.titleName] as String?,
        json[DocumentDetailsFields.descriptionId] as int?,
        json[DocumentDetailsFields.descriptionName] as String?,
        json[DocumentDetailsFields.displayOrder] as int?);
}
