final String tableDescription = "tblDescription";

class DescriptionFields {
  static final List<String> values = [
    descriptionId,descriptionName
  ];

  static final String descriptionId = "descriptionId";
  static final String descriptionName = "descriptionName";
}

class Description {
  int? descriptionId;
  String? descriptionName;

  Description(this.descriptionName);

  Description.withOutDetails();

  Description.withDetails(this.descriptionId,this.descriptionName);

  int? get getdescriptionId => descriptionId;
  String? get getdescriptionName => descriptionName;

  set setDescriptionId(int? descriptionId) {
    this.descriptionId = descriptionId;
  }

  set setDescriptionName(String? descriptionName) {
    this.descriptionName = descriptionName;
  }

  Map<String, Object?> toMap() => {
    DescriptionFields.descriptionId : descriptionId,
    DescriptionFields.descriptionName : descriptionName
  };

  Description copy({
    int? descriptionId,
    String? descriptionName
  }) => Description.withDetails(descriptionId, descriptionName);

  static Description fromJson(Map<String,Object?> json) => Description.withDetails(
      json[DescriptionFields.descriptionId] as int,
      json[DescriptionFields.descriptionName] as String,);
}