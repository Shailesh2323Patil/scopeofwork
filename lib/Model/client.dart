final String tableClients = "tblClients";

class ClientFields {
  static final List<String> values = [
    clientId,clientName,contactNumber,alternateNumber,emailId
  ];

  static final String clientId = "clientId";
  static final String clientName = "clientName";
  static final String contactNumber = "contactNumber";
  static final String alternateNumber = "alternateNumber";
  static final String emailId = "emailId";
}

class Client {
  int? clientId;
  String? clientName;
  int? contactNumber;
  int? alternateNumber;
  String? emailId;

  Client(this.clientName,this.contactNumber,this.alternateNumber,this.emailId);

  Client.withDetails(this.clientId,this.clientName,this.contactNumber,this.alternateNumber,this.emailId);

  int? get getClientId => clientId;
  String? get getClientName => clientName;
  int? get getContactNumber => contactNumber;
  int? get getAlternateNumber => alternateNumber;
  String? get getEmailId => emailId;

  set setClientId(int? clientId) {
    this.clientId = clientId;
  }

  set setClientName(String? clientName) {
    this.clientName = clientName;
  }

  set setContactNumber(int? contactNumber) {
    this.contactNumber = contactNumber;
  }

  set setAlternateNumber(int? alternateNumber) {
    this.alternateNumber = alternateNumber;
  }

  set setEmailId(String? emailId) {
    this.emailId = emailId;
  }

  Map<String, Object?> toMap() => {
    ClientFields.clientId : clientId,
    ClientFields.clientName : clientName,
    ClientFields.contactNumber : contactNumber,
    ClientFields.alternateNumber : alternateNumber,
    ClientFields.emailId : emailId
  };

  Client copy({
    int? clientId,
    String? clientName,
    int? contactNumber,
    int? alternateNumber,
    String? emailId
  }) => Client.withDetails(clientId, clientName, contactNumber, alternateNumber, emailId);

  static Client fromJson(Map<String,Object?> json) => Client.withDetails(
      json[ClientFields.clientId] as int,
      json[ClientFields.clientName] as String,
      json[ClientFields.contactNumber] as int,
      json[ClientFields.alternateNumber] as int,
      json[ClientFields.emailId] as String);
}