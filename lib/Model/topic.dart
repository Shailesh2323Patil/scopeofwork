final String tableTopic = "tblTopics";

class TopicFields {
  static final List<String> values = [
    topicId,topicName
  ];

  static final String topicId = "topicId";
  static final String topicName = "topicName";
}

class Topic {
  int? topicId;
  String? topicName;

  Topic(this.topicName);

  Topic.withOutDetails();

  Topic.withDetails(this.topicId,this.topicName);

  int? get gettopicId => topicId;
  String? get gettopicName => topicName;

  set setTopicId(int? topicId) {
    this.topicId = topicId;
  }

  set setTopicName(String? topicName) {
    this.topicName = topicName;
  }

  Map<String, Object?> toMap() => {
    TopicFields.topicId : topicId,
    TopicFields.topicName : topicName
  };

  Topic copy({
    int? topicId,
    String? topicName
  }) => Topic.withDetails(topicId, topicName);

  static Topic fromJson(Map<String,Object?> json) => Topic.withDetails(
      json[TopicFields.topicId] as int,
      json[TopicFields.topicName] as String,);

  @override
  String toString() {
    // TODO: implement toString
    return topicName.toString();
  }
}