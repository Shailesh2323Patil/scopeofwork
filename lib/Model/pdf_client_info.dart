class ProjectClientInfoFields {
  static final List<String> values = [
    clientId,clientName,contactNumber,alternateNumber,emailId,projectId,projectName,date
  ];

  static final String clientId = "clientId";
  static final String clientName = "clientName";
  static final String contactNumber = "contactNumber";
  static final String alternateNumber = "alternateNumber";
  static final String emailId = "emailId";
  static final String projectId = "projectId";
  static final String projectName = "projectName";
  static final String date = "date";
}

class ProjectClientInfo {
  int? clientId;
  String? clientName;
  int? contactNumber;
  int? alternateNumber;
  String? emailId;
  int? projectId;
  String? projectName;
  String? date;

  ProjectClientInfo(this.clientName,this.contactNumber,this.alternateNumber,this.emailId,this.projectId,this.projectName,this.date);

  ProjectClientInfo.withDetails(this.clientId,this.clientName,this.contactNumber,this.alternateNumber,this.emailId,this.projectId,this.projectName,this.date);

  static ProjectClientInfo fromJson(Map<String,Object?> json) => ProjectClientInfo.withDetails(
      json[ProjectClientInfoFields.clientId] as int,
      json[ProjectClientInfoFields.clientName] as String,
      json[ProjectClientInfoFields.contactNumber] as int,
      json[ProjectClientInfoFields.alternateNumber] as int,
      json[ProjectClientInfoFields.emailId] as String,
      json[ProjectClientInfoFields.projectId] as int,
      json[ProjectClientInfoFields.projectName] as String,
      json[ProjectClientInfoFields.date] as String);
}