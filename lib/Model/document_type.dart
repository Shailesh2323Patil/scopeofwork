final String tableDocumentType = "tblDocumentType";

class DocumentTypeFields {
  static final List<String> values = [
    documentTypeId,documentTypeName
  ];

  static final String documentTypeId = "documentTypeId";
  static final String documentTypeName = "documentTypeName";
}

class DocumentType {
  int? documentTypeId;
  String? documentTypeName;

  DocumentType(this.documentTypeName);

  DocumentType.withDetails(this.documentTypeId,this.documentTypeName);

  int? get getDocumentTypeId => documentTypeId;

  String? get getDocumentTypeName => documentTypeName;

  set setDocumentTypeId(int? documentTypeId) {
    this.documentTypeId = documentTypeId;
  }

  set setDocumentTypeName(String? documentTypeName) {
    this.documentTypeName = documentTypeName;
  }

  Map<String, Object?> toMap() => {
    DocumentTypeFields.documentTypeId : documentTypeId,
    DocumentTypeFields.documentTypeName : documentTypeName
  };

  DocumentType copy({
    int? documentTypeId,
    String? documentTypeName
  }) => DocumentType.withDetails(documentTypeId, documentTypeName);

  static DocumentType fromJson(Map<String,Object?> json) => DocumentType.withDetails(
    json[DocumentTypeFields.documentTypeId] as int,
    json[DocumentTypeFields.documentTypeName] as String,);
}