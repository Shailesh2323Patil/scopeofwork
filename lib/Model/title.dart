final String tableTitle = "tblTitles";

class TitleFields {
  static final List<String> values = [
    titleId,titleName
  ];

  static final String titleId = "titleId";
  static final String titleName = "titleName";
}

class Title_ {
  int? titleId;
  String? titleName;

  Title_(this.titleName);

  Title_.withOutDetails();

  Title_.withDetails(this.titleId,this.titleName);

  int? get gettitleId => titleId;
  String? get gettitleName => titleName;

  set setTitleId(int? titleId) {
    this.titleId = titleId;
  }

  set setTitleName(String? titleName) {
    this.titleName = titleName;
  }

  Map<String, Object?> toMap() => {
    TitleFields.titleId : titleId,
    TitleFields.titleName : titleName
  };

  Title_ copy({
    int? titleId,
    String? titleName
  }) => Title_.withDetails(titleId, titleName);

  static Title_ fromJson(Map<String,Object?> json) => Title_.withDetails(
      json[TitleFields.titleId] as int,
      json[TitleFields.titleName] as String,);
}