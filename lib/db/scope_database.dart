import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:scope_of_work/model/description.dart';
import 'package:scope_of_work/model/document_details.dart';
import 'package:scope_of_work/model/client.dart';
import 'package:scope_of_work/model/document_type.dart';
import 'package:scope_of_work/model/document_version.dart';
import 'package:scope_of_work/model/pdf_client_info.dart';
import 'package:scope_of_work/model/project.dart';
import 'package:scope_of_work/model/title.dart';
import 'package:scope_of_work/model/topic.dart';
import 'package:sqflite/sqflite.dart';

class ScopeDatabase {
  static final ScopeDatabase instance = ScopeDatabase._init();

  ScopeDatabase._init();

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB("scope_of_work.db");
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idType = "INTEGER PRIMARY KEY AUTOINCREMENT";
    final idTypeInt = "INTEGER";
    final textType = "TEXT NOT NULL";
    final boolType = "BOOLEAN NOT NULL";
    final integerType = "INTEGER NOT NULL";
    final identity = "INTEGER IDENTITY(1,1)";

    await db.execute(''
        'CREATE TABLE $tableClients ( '
        '${ClientFields.clientId} $idType ,'
        '${ClientFields.clientName} $textType ,'
        '${ClientFields.contactNumber} $integerType ,'
        '${ClientFields.alternateNumber} $integerType ,'
        '${ClientFields.emailId} $textType '
        ' )');

    await db.execute(''
        'CREATE TABLE $tableProject ( '
        '${ProjectFields.projectId} $idType ,'
        '${ProjectFields.clientId} $idTypeInt ,'
        '${ProjectFields.projectName} $textType , '
        '${ProjectFields.date} $textType '
        ' )');

    await db.execute(''
        'CREATE TABLE $tableDocumentType ( '
        '${DocumentTypeFields.documentTypeId} $idType ,'
        '${DocumentTypeFields.documentTypeName} $textType'
        ' )');

    /* This entry for by default selection */
    await db.execute(''
        'Insert Into $tableDocumentType ( '
        '${DocumentTypeFields.documentTypeName} '
        ' ) VALUES ("Quotation")');

    await db.execute(''
        'CREATE TABLE $tableDocumentVersion ( '
        '${DocumentVersionFields.documentVersionId} $idType ,'
        '${DocumentVersionFields.documentVersionName} $textType , '
        '${DocumentVersionFields.projectId} $idTypeInt , '
        '${DocumentVersionFields.documentTypeId} $idTypeInt , '
        '${DocumentVersionFields.createdDate} $textType , '
        '${DocumentVersionFields.updatedDate} $textType '
        ' )');

    await db.execute(''
        'CREATE TABLE $tableDocumentDetails ( '
        '${DocumentDetailsFields.docDetailsId} $idType , '
        '${DocumentDetailsFields.documentVersionId} $idTypeInt , '
        '${DocumentDetailsFields.topicId} $idTypeInt , '
        '${DocumentDetailsFields.titleId} $idTypeInt , '
        '${DocumentDetailsFields.descriptionId} $idTypeInt , '
        '${DocumentDetailsFields.displayOrder} $identity '
        ' )');

    await db.execute(''
        'CREATE TABLE $tableTopic ( '
        '${TopicFields.topicId} $idType , '
        '${TopicFields.topicName} $textType'
        ' )');

    await db.execute(''
        'CREATE TABLE $tableTitle ( '
        '${TitleFields.titleId} $idType , '
        '${TitleFields.titleName} $textType'
        ' )');

    await db.execute(''
        'CREATE TABLE $tableDescription ( '
        '${DescriptionFields.descriptionId} $idType , '
        '${DescriptionFields.descriptionName} $textType'
        ' )');
  }
  /*------------------------------------- First Form Table Start ----------------------------------------*/

  /* Project Start*/
  Future<Project> insertProjectData(Project project) async {
    final db = await instance.database;
    final id = await db.insert(tableProject, project.toMap());
    return project.copy(projectId: id);
  }

  Future<Project> updateProjectData(Project project) async {
    final db = await instance.database;
    final id = await db.update(tableProject, project.toMap(),
        where: '${ProjectFields.projectId} = ?',
        whereArgs: [project.projectId]);
    return readProject(id);
  }

  Future<Project> readProject(int id) async {
    final db = await instance.database;

    final maps = await db.query(tableProject,
        columns: ProjectFields.values,
        where: '${ProjectFields.projectId} = ?',
        whereArgs: [id]);

    if (maps.isNotEmpty) {
      return Project.fromJson(maps.first);
    } else {
      throw Exception('Client Id $id Not Found');
    }
  }

  Future<List<Project>> readAllProject(int clientId) async {
    final db = await instance.database;

    final result = await db.query(
      tableProject,
      orderBy: '${ProjectFields.projectName} ASC',
      where: '${ProjectFields.clientId} = ?',
      whereArgs: [clientId]
    );

    debugPrint(result.toString());

    return result.map((json) => Project.fromJson(json)).toList();
  }

  /* Project End*/

  /* Client Start*/
  Future<Client> insertData(Client client) async {
    final db = await instance.database;
    final id = await db.insert(tableClients, client.toMap());
    return client.copy(clientId: id);
  }

  Future<int> updateData(Client client) async {
    final db = await instance.database;

    return db.update(tableClients, client.toMap(),
        where: '${ClientFields.clientId} = ?', whereArgs: [client.clientId]);
  }

  Future<Client> readClient(int id) async {
    final db = await instance.database;

    final maps = await db.query(tableClients,
        columns: ClientFields.values,
        where: '${ClientFields.clientId} = ?',
        whereArgs: [id]);

    if (maps.isNotEmpty) {
      return Client.fromJson(maps.first);
    } else {
      throw Exception('Client Id $id Not Found');
    }
  }

  Future<List<Client>> readAllClients() async {
    final db = await instance.database;

    final result = await db.query(
      tableClients,
      orderBy: '${ClientFields.clientName} ASC',
    );

    debugPrint(result.toString());

    return result.map((json) => Client.fromJson(json)).toList();
  }
  /* Client End*/

  /* Document Type Start*/
  Future<int> insertDocumentTypeData(DocumentType documentType) async {
    final db = await instance.database;
    final id = await db.insert(tableDocumentType, documentType.toMap());
    return id != 0 ? id : 0;
  }

  Future<int> updateDocumentTypeData(DocumentType documentType) async {
    final db = await instance.database;

    return db.update(tableDocumentType, documentType.toMap(), where: '${DocumentTypeFields.documentTypeId} = ?', whereArgs: [documentType.documentTypeId]);
  }

  Future<DocumentType> readDocumentType(int id) async {
    final db = await instance.database;

    final maps = await db.query(tableDocumentType,
        columns: DocumentTypeFields.values,
        where: '${DocumentTypeFields.documentTypeId} = ?',
        whereArgs: [id]);

    if (maps.isNotEmpty) {
      return DocumentType.fromJson(maps.first);
    } else {
      throw Exception('DocumentType Id $id Not Found');
    }
  }

  Future<List<DocumentType>> readAllDocumentTypes() async {
    final db = await instance.database;

    final result = await db.query(
      tableDocumentType,
      orderBy: '${DocumentTypeFields.documentTypeName} ASC',
    );

    debugPrint(result.toString());

    return result.map((json) => DocumentType.fromJson(json)).toList();
  }
  /* Document Type End*/

  /* Document Version Start*/
  Future<DocumentVersion> insertDocumentVersionData(DocumentVersion documentVersion) async {
    final db = await instance.database;
    final id = await db.insert(tableDocumentVersion, documentVersion.toMap());
    return readDocumentVersion(id);
  }

  Future<DocumentVersion> updateDocumentVersionData(DocumentVersion documentVersion) async {
    final db = await instance.database;
    final id = await db.update(tableDocumentVersion, documentVersion.toMap(), where: '${DocumentVersionFields.documentVersionId} = ?', whereArgs: [documentVersion.documentVersionId]);
    return readDocumentVersion(id);
  }

  Future<DocumentVersion> readDocumentVersion(int id) async {
    final db = await instance.database;

    final maps = await db.query(tableDocumentVersion,
        columns: DocumentVersionFields.values,
        where: '${DocumentVersionFields.documentVersionId} = ?',
        whereArgs: [id]);

    if (maps.isNotEmpty) {
      return DocumentVersion.fromJson(maps.first);
    } else {
      throw Exception('DocumentVersion Id $id Not Found');
    }
  }

  Future<List<DocumentVersion>> readAllDocumentVersion(int projectId) async {
    final db = await instance.database;

    final result = await db.query(
      tableDocumentVersion,
      where: '${DocumentVersionFields.projectId} = ?',
      whereArgs: [projectId],
      orderBy: '${DocumentVersionFields.documentVersionName} ASC',
    );

    debugPrint(result.toString());

    return result.map((json) => DocumentVersion.fromJson(json)).toList();
  }
  /* Document Version End*/

  /* Document Details Start */
  Future<DocumentDetails> insertDocumentDetails(DocumentDetails documentDetails) async {
    final db = await instance.database;

    /* Select Previous Display Order Number */
    String query = "SELECT * FROM $tableDocumentDetails ORDER BY ${DocumentDetailsFields.displayOrder} DESC LIMIT 1";
    final maps = await db.rawQuery(query);

    if(maps.isNotEmpty) {
      DocumentDetails countDetails = DocumentDetails.fromJson(maps.first);
      int count = countDetails.getDisplayOrder! + 1;
      documentDetails.displayOrder = count;
    }
    else {
      documentDetails.displayOrder = 1;
    }

    /* Insert Record */
    final id = await db.insert(tableDocumentDetails, documentDetails.toMap());

    /* Read the same record */
    final maps2 = await db.query(tableDocumentDetails,
        columns: DocumentDetailsFields.values,
        where: '${DocumentDetailsFields.docDetailsId} = ?',
        whereArgs: [id]);

        if (maps2.isNotEmpty) {
          return DocumentDetails.fromJson(maps2.first);
        } else {
          throw Exception('DocumentDetails Id $id Not Found');
        }
  }

  Future<DocumentDetails> updateDocumentDetails(DocumentDetails documentDetails) async {
    final db = await instance.database;

    final id = await db.update(tableDocumentDetails, documentDetails.toMap(),
        where: '${DocumentDetailsFields.docDetailsId} = ?', whereArgs: [documentDetails.docDetailsId]);

    return readDocumentDetails(id);
  }

  Future updateAllDocumentDetails(List<DocumentDetails> listDocumentDetails) async {
    final db = await instance.database;

    for(int index = 0; index < listDocumentDetails.length; index++){
      String query = "UPDATE $tableDocumentDetails SET ${DocumentDetailsFields.displayOrder} = ${index + 1} WHERE ${DocumentDetailsFields.docDetailsId} = ${listDocumentDetails[index].getDocDetailsId}";
      final id = await db.rawQuery(query);
    }
  }

  Future<DocumentDetails> readDocumentDetails(int id) async {
    final db = await instance.database;

    final maps = await db.query(tableDocumentDetails,
        columns: DocumentDetailsFields.values,
        where: '${DocumentDetailsFields.docDetailsId} = ?',
        whereArgs: [id]);

    if (maps.isNotEmpty) {
      return DocumentDetails.fromJson(maps.first);
    } else {
      throw Exception('DocumentDetails Id $id Not Found');
    }
  }

  Future<List<DocumentDetails>> readAllDocumentDetails(int documentVersionId) async {
    final db = await instance.database;

    //String query = " SELECT * FROM "+tableDocumentDetails;

    String query = "SELECT "+ tableDocumentDetails + "." + DocumentDetailsFields.docDetailsId +","+ tableDocumentDetails + "." +DocumentDetailsFields.documentVersionId +
        ","+ tableDocumentDetails + "." +DocumentDetailsFields.topicId +
        ","+ tableTopic + "." + TopicFields.topicName +","+ tableDocumentDetails + "." + DocumentDetailsFields.titleId +","+ tableTitle + "." + TitleFields.titleName +
        ","+ tableDocumentDetails + "." + DocumentDetailsFields.descriptionId + "," + tableDescription + "." + DescriptionFields.descriptionName + ", "
        ""+ tableDocumentDetails + "." + DocumentDetailsFields.displayOrder +" "
        "FROM ((("+ tableDocumentDetails +" "
        "LEFT JOIN " + tableTopic + " ON " + tableDocumentDetails + "." + DocumentDetailsFields.topicId +" = " + tableTopic + "." + TopicFields.topicId +" ) "
        "LEFT JOIN " + tableTitle + " ON " + tableDocumentDetails + "." + DocumentDetailsFields.titleId +" = " + tableTitle + "." + TitleFields.titleId +" ) "
        "LEFT JOIN " + tableDescription + " ON " + tableDocumentDetails + "." +DocumentDetailsFields.descriptionId +" = "+ tableDescription + "." + DescriptionFields.descriptionId + " ) "
        "Where "+ tableDocumentDetails + "." + DocumentDetailsFields.documentVersionId +" =  "+ documentVersionId.toString() +
        " ORDER BY "+ tableDocumentDetails + "." + DocumentDetailsFields.displayOrder +" ASC ";

    //debugPrint("------------------------------");
    //debugPrint(query);
    //debugPrint("------------------------------");

    final result = await db.rawQuery(query);

    // debugPrint("XXXXXXXXXXX");
    // debugPrint(result.toString());
    // debugPrint("XXXXXXXXXXX");

    return result.map((json) => DocumentDetails.fromFullJson(json)).toList();
  }

  Future<List<ProjectClientInfo>> readProjectClientInfo(int documentVersionId) async {
    final db = await instance.database;

    String query = "SELECT tblClients.clientId, tblClients.clientName, tblClients.contactNumber, tblClients.alternateNumber, tblClients.emailId, "
        " tblProjects.projectId, tblProjects.projectName, tblProjects.date "
        " FROM ((tblDocumentVersion LEFT JOIN tblProjects ON tblDocumentVersion.projectId = tblProjects.projectId ) "
        " LEFT JOIN tblClients ON tblProjects.clientId = tblClients.clientId ) "
        " Where tblDocumentVersion.documentVersionId = $documentVersionId ";

    final result = await db.rawQuery(query);

    //debugPrint("XXXXXXXXXXX");
    //debugPrint(result.toString());
    //debugPrint("XXXXXXXXXXX");

    return result.map((json) => ProjectClientInfo.fromJson(json)).toList();
  }

  Future<bool> deleteDocumentDetails(int? id) async {
    final db = await instance.database;

    final count = await db.rawDelete("DELETE FROM $tableDocumentDetails WHERE ${DocumentDetailsFields.docDetailsId} = ?",[id]);

    if (count == 1) {
      return true;
    } else {
      throw Exception('DocumentDetails Id $id Not Found');
    }
  }

  Future<List<DocumentDetails>> readAllDocumentDetailsForCopy(int? documentVersionId) async {
    final db = await instance.database;
    String query = " SELECT * FROM "+tableDocumentDetails+" Where ${ DocumentDetailsFields.documentVersionId } = $documentVersionId";
    final result = await db.rawQuery(query);
    return result.map((json) => DocumentDetails.fromJson(json)).toList();
  }

  Future insertAllDocumentVersionAfterCopy(List<DocumentDetails> listDocumentDetails, DocumentVersion newDocumentVersion) async {

    final db = await instance.database;

    for(int index = 0; index < listDocumentDetails.length; index++) {

      /* Fetch Single element of DocumentDetails */
      DocumentDetails documentDetails = listDocumentDetails[index];

      /* Assign new Document Details Version Id */
      DocumentDetails insertDocumentDetails = new DocumentDetails(newDocumentVersion.documentVersionId, documentDetails.topicId, documentDetails.titleId, documentDetails.descriptionId, documentDetails.displayOrder);

      /* Insert Record */
      final id = await db.insert(tableDocumentDetails, insertDocumentDetails.toMap());
    }
  }

  Future deleteDocumentWithAllDocumentDetails(int? documentVersionId) async {
    final db = await instance.database;

    await db.rawDelete("DELETE FROM $tableDocumentVersion WHERE ${DocumentVersionFields.documentVersionId} = ?",[documentVersionId]);

    await db.rawDelete("DELETE FROM $tableDocumentDetails WHERE ${DocumentDetailsFields.documentVersionId} = ?",[documentVersionId]);

    List<DocumentDetails> list = await readAllDocumentDetailsForCopy(documentVersionId);

    debugPrint("List Data "+list.length.toString());
  }

  /* Document Details End */

  /* Topic Start*/
  Future<int> insertTopicData(Topic topic) async {
    final db = await instance.database;
    final id = await db.insert(tableTopic, topic.toMap());
    return id != 0 ? id : 0;
  }

  Future<int> updateTopicData(Topic topic) async {
    final db = await instance.database;

    return db.update(tableTopic, topic.toMap(), where: '${TopicFields.topicId} = ?', whereArgs: [topic.topicId]);
  }

  Future<Topic> readTopic(int id) async {
    final db = await instance.database;

    final maps = await db.query(tableTopic,
        columns: TopicFields.values,
        where: '${TopicFields.topicId} = ?',
        whereArgs: [id]);

    if (maps.isNotEmpty) {
      return Topic.fromJson(maps.first);
    } else {
      throw Exception('Topic Id $id Not Found');
    }
  }

  Future<List<Topic>> readAllTopics() async {
    final db = await instance.database;

    final result = await db.query(
      tableTopic,
      orderBy: '${TopicFields.topicName} ASC',
    );

    debugPrint(result.toString());

    return result.map((json) => Topic.fromJson(json)).toList();
  }
  /* Topic End*/

  /* Title Start*/
  Future<int> insertTitleData(Title_ title_) async {
    final db = await instance.database;
    final id = await db.insert(tableTitle, title_.toMap());
    return id != 0 ? id : 0;
  }

  Future<int> updateTitleData(Title_ title_) async {
    final db = await instance.database;
    return db.update(tableTitle, title_.toMap(), where: '${TitleFields.titleId} = ?', whereArgs: [title_.titleId]);
  }

  Future<Title_> readTitle(int id) async {
    final db = await instance.database;

    final maps = await db.query(tableTitle,
        columns: TitleFields.values,
        where: '${TitleFields.titleId} = ?',
        whereArgs: [id]);

    if (maps.isNotEmpty) {
      return Title_.fromJson(maps.first);
    } else {
      throw Exception('Title Id $id Not Found');
    }
  }

  Future<List<Title_>> readAllTitle() async {
    final db = await instance.database;

    final result = await db.query(
      tableTitle,
      orderBy: '${TitleFields.titleName} ASC',
    );

    debugPrint(result.toString());

    return result.map((json) => Title_.fromJson(json)).toList();
  }
  /* Title End*/

  /* Description Start*/
  Future<int> insertDescriptionData(Description description) async {
    final db = await instance.database;
    final id = await db.insert(tableDescription, description.toMap());
    return id != 0 ? id : 0;
  }

  Future<int> updateDescriptionData(Description description) async {
    final db = await instance.database;
    return db.update(tableDescription, description.toMap(), where: '${DescriptionFields.descriptionId} = ?', whereArgs: [description.descriptionId]);
  }

  Future<Description> readDescription(int id) async {
    final db = await instance.database;

    final maps = await db.query(tableDescription,
        columns: DescriptionFields.values,
        where: '${DescriptionFields.descriptionId} = ?',
        whereArgs: [id]);

    if (maps.isNotEmpty) {
      return Description.fromJson(maps.first);
    } else {
      throw Exception('Description Id $id Not Found');
    }
  }

  Future<List<Description>> readAllDescription() async {
    final db = await instance.database;

    final result = await db.query(
      tableDescription,
      orderBy: '${DescriptionFields.descriptionName} ASC',
    );

    debugPrint(result.toString());

    return result.map((json) => Description.fromJson(json)).toList();
  }
  /* Description End*/

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
