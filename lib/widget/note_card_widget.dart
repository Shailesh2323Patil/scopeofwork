import 'package:flutter/material.dart';
import 'package:scope_of_work/model/client.dart';
import 'package:scope_of_work/page/project/project_list_page.dart';

final _lightColors = [
  Colors.amber.shade300,
  Colors.lightGreen.shade300,
  Colors.lightBlue.shade300,
  Colors.orange.shade300,
  Colors.pinkAccent.shade100,
  Colors.tealAccent.shade100
];

// To return different height for different widgets
double getMinHeight(int index) {
  switch (index % 4) {
    case 0:
      return 100;
    case 1:
      return 150;
    case 2:
      return 150;
    case 3:
      return 100;
    default:
      return 100;
  }
}

class NoteCardWidget extends StatelessWidget {
  final Client client;
  final int index;
  final BuildContext buildContext;

  NoteCardWidget(
      {Key? key,
      required this.client,
      required this.index,
      required this.buildContext})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Pick colors from accent colors based on index
    final color = _lightColors[index % _lightColors.length];
    final minHeight = getMinHeight(index);

    return Card(
      color: color,
      child: Container(
        constraints: BoxConstraints(minHeight: minHeight),
        padding: EdgeInsets.all(8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(client.getClientName!,
                style: TextStyle(
                    color: Colors.grey.shade700,
                    fontSize: 20,
                    fontWeight: FontWeight.bold)),
            SizedBox(height: 4),
            Text(
              client.getContactNumber.toString(),
              style: TextStyle(
                  color: Colors.grey.shade700,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 4),
            Text(
              client.getEmailId!,
              style: TextStyle(
                  color: Colors.grey.shade700,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 4),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              width: double.infinity,
              child: ElevatedButton.icon(
                onPressed: () {
                  nextPage(client.clientId);
                },
                label: Flexible(
                    child: Text('Open Projects', maxLines: 1, overflow: TextOverflow.ellipsis)),
                icon: Icon(Icons.add),
                style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).accentColor,
                    onPrimary: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }

  void nextPage(int? clientId) async {
    bool result = await Navigator.of(buildContext).push(MaterialPageRoute(builder: (buildContext) => ProjectListPage(clientId: clientId)));
  }
}
