import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:scope_of_work/utils/constants.dart';

class DialogBoxSelectType extends StatefulWidget {
  SelectTypeTopicName? selectTypeTopicName;
  SelectTypeTitleName? selectTypeTitleName;
  SelectTypeDescriptionName? selectTypeDescriptionName;

  DialogBoxSelectType({required this.selectTypeTopicName,required this.selectTypeTitleName,required this.selectTypeDescriptionName});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DialogBoxState(selectTypeTopicName: selectTypeTopicName,selectTypeTitleName: selectTypeTitleName,selectTypeDescriptionName: selectTypeDescriptionName);
  }
}

class DialogBoxState extends State<DialogBoxSelectType> {
  SelectTypeTopicName? selectTypeTopicName;
  SelectTypeTitleName? selectTypeTitleName;
  SelectTypeDescriptionName? selectTypeDescriptionName;

  DialogBoxState({required this.selectTypeTopicName,required this.selectTypeTitleName,required this.selectTypeDescriptionName});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Constants.padding)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.80,
        height: MediaQuery.of(context).size.height * 0.30,
        padding: EdgeInsets.all(Constants.padding),
        margin: EdgeInsets.all(Constants.padding),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Constants.padding),
            boxShadow: [
              BoxShadow(
                  color: Colors.black, offset: Offset(0, 5), blurRadius: 5)
            ]),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                  selectTypeTopicName!();
                },
                child: Text(
                  "ADD Topic Name",
                  style: TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.w600,
                      color: Colors.redAccent),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    selectTypeTitleName!();
                  },
                  child: Text(
                    "ADD Title Name",
                    style: TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.w600,
                        color: Colors.green.shade700),
                    textAlign: TextAlign.center,
                  )),
              SizedBox(
                height: 15,
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    selectTypeDescriptionName!();
                  },
                  child: Text(
                    "ADD Description",
                    style: TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.w600,
                        color: Colors.blue.shade700),
                    textAlign: TextAlign.center,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

typedef SelectTypeTopicName = void Function();
typedef SelectTypeTitleName = void Function();
typedef SelectTypeDescriptionName = void Function();
