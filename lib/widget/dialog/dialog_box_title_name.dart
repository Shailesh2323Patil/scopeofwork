import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/title.dart';
import 'package:scope_of_work/utils/constants.dart';
import 'package:scope_of_work/utils/string_class.dart';
import '../type_ahead_form.dart';

class DialogBoxTitle extends StatefulWidget {

  final TopicNameCallBack titleNameCallBack;

  DialogBoxTitle({required this.titleNameCallBack});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DialogBoxTitleState(titleNameCallBack : titleNameCallBack);
  }
}

class DialogBoxTitleState extends State<DialogBoxTitle> {

  final TopicNameCallBack titleNameCallBack;

  DialogBoxTitleState({required this.titleNameCallBack});

  @override
  void initState() {
    refreshTitleData();
  }

  /* Title Data Start */
  final suggestionTitleController = TextEditingController();
  static late List<String> titleStringList = [];
  static late List<Title_> titleList = [];

  static Future refreshTitleData() async {
    titleList = await ScopeDatabase.instance.readAllTitle();
    titleStringList.clear();
    for (int i = 0; i < titleList.length; i++) {
      titleStringList.add(titleList[i].gettitleName.toString());
    }
  }

  Title_ selectTitle = new Title_.withOutDetails();

  Future<int> addDataInTitle(var titleName) async {
    Title_ title = new Title_(titleName);
    return await ScopeDatabase.instance.insertTitleData(title);
  }
  /* Title Data End */

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(Constants.padding)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    TextStyle? textStyle = Theme.of(context).textTheme.headline6;

    return Container(
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * 0.30,
      padding: EdgeInsets.all(Constants.padding),
      margin: EdgeInsets.all(Constants.padding),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.circular(Constants.padding),
          boxShadow: [
            BoxShadow(color: Colors.black, offset: Offset(0, 5), blurRadius: 5)
          ]),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // Edit Title
          Container(
              margin: EdgeInsets.only(top: 0.0),
              child: TypeAheadForm(
                  textStyle : textStyle,
                  textEditingController : suggestionTitleController,
                  labelText : StringClass.textTitle,
                  suggestionsFunction : getSuggestions,
                  refreshData : refreshTitleData,
                  addData : addDataInTitle,
                  list : titleStringList,
                  getSelectedSuggestion: getSelectedSuggestion)),
          Expanded(child:Container()),
          Center(
            child: Align(
                alignment: Alignment.bottomCenter,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).accentColor,
                      onPrimary: Colors.white),
                  child: Text(
                    StringClass.textSubmit,
                    textScaleFactor: 1.5,
                  ),
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).pop();
                      titleNameCallBack(selectTitle);
                    });
                  },
                )
            ),
          )
        ],
      ),
    );
  }

  Future<List<String>> getSuggestions(String query, List<String> list) async {
    return await List.of(list).where((suggestion) {
      final suggestionLower = suggestion.toLowerCase();
      final queryLower = query.toLowerCase();

      return suggestionLower.contains(queryLower);
    }).toList();
  }

  void getSelectedSuggestion(String suggestion) {
    final title = titleList.singleWhere((element) => element.gettitleName == suggestion);
    if (title == null) {
      Fluttertoast.showToast(msg: StringClass.textPleaseSelectTopicNameAgain);
    } else {
      selectTitle = title;
    }
  }
}

typedef TopicNameCallBack = void Function(Title_ title_);