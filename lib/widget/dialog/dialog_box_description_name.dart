import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/description.dart';
import 'package:scope_of_work/utils/constants.dart';
import 'package:scope_of_work/utils/string_class.dart';

import '../type_ahead_form.dart';

class DialogBoxDescription extends StatefulWidget {

  final DescriptionNameCallBack descriptionNameCallBack;

  DialogBoxDescription({required this.descriptionNameCallBack});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DialogBoxDescriptionState(descriptionNameCallBack : descriptionNameCallBack);
  }
}

class DialogBoxDescriptionState extends State<DialogBoxDescription> {

  final DescriptionNameCallBack descriptionNameCallBack;

  DialogBoxDescriptionState({required this.descriptionNameCallBack});

  @override
  void initState() {
    refreshDescriptionData();
  }

  /* Description Data Start */
  final suggestionDescriptionController = TextEditingController();
  static late List<String> descriptionStringList = [];
  static late List<Description> descriptionList = [];
  Description selectDescription = new Description.withOutDetails();

  static Future refreshDescriptionData() async {
    descriptionList = await ScopeDatabase.instance.readAllDescription();
    descriptionStringList.clear();
    for (int i = 0; i < descriptionList.length; i++) {
      descriptionStringList.add(descriptionList[i].getdescriptionName.toString());
    }
  }

  Future<int> addDataInDescription(var descriptionName) async {
    Description description = new Description(descriptionName);
    return await ScopeDatabase.instance.insertDescriptionData(description);
  }
  /* Description Data End */

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Constants.padding)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    TextStyle? textStyle = Theme.of(context).textTheme.headline6;

    return Container(
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * 0.30,
      padding: EdgeInsets.all(Constants.padding),
      margin: EdgeInsets.all(Constants.padding),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.circular(Constants.padding),
          boxShadow: [
            BoxShadow(color: Colors.black, offset: Offset(0, 5), blurRadius: 5)
          ]),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // Edit Description
          Container(
              margin: EdgeInsets.only(top: 0.0),
              child: TypeAheadForm(
                  textStyle: textStyle,
                  textEditingController : suggestionDescriptionController,
                  labelText : StringClass.textDescription,
                  suggestionsFunction : getSuggestions,
                  refreshData : refreshDescriptionData,
                  addData : addDataInDescription,
                  list : descriptionStringList,
                  getSelectedSuggestion : getSelectedSuggestion)),
          Expanded(child:Container()),
          Center(
            child: Align(
                alignment: Alignment.bottomCenter,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).accentColor,
                      onPrimary: Colors.white),
                  child: Text(
                    'Submit',
                    textScaleFactor: 1.5,
                  ),
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).pop();
                      descriptionNameCallBack(selectDescription);
                    });
                  },
                )
            ),
          )
        ],
      ),
    );
  }

  Future<List<String>> getSuggestions(String query, List<String> list) async {
    return await List.of(list).where((suggestion) {
      final suggestionLower = suggestion.toLowerCase();
      final queryLower = query.toLowerCase();

      return suggestionLower.contains(queryLower);
    }).toList();
  }

  void getSelectedSuggestion(String suggestion) {
    final description = descriptionList.singleWhere((element) => element.getdescriptionName == suggestion);
    if (description == null) {
      Fluttertoast.showToast(msg: StringClass.textPleaseSelectTopicNameAgain);
    } else {
      selectDescription = description;
    }
  }
}

typedef DescriptionNameCallBack = void Function(Description description);
