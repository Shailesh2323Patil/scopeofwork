import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/topic.dart';
import 'package:scope_of_work/utils/constants.dart';
import 'package:scope_of_work/utils/string_class.dart';

import '../type_ahead_form.dart';


class DialogBoxTopic extends StatefulWidget {

  final TopicNameCallBack topicNameCallBack;

  DialogBoxTopic({required this.topicNameCallBack});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DialogBoxTopicState(topicNameCallBack: topicNameCallBack);
  }
}

class DialogBoxTopicState extends State<DialogBoxTopic> {

  final TopicNameCallBack topicNameCallBack;

  DialogBoxTopicState({required this.topicNameCallBack});

  /* Topic Data Start */
  final suggestionTopicController = TextEditingController();
  static late List<String> topicStringList = [];
  static late List<Topic> topicList = [];
  bool isLoading = false;
  String insertionId = "0";

  Topic selectedTopic = new Topic.withOutDetails();

  @override
  void initState() {
    super.initState();
    refreshTopicData();
  }

  Future refreshTopicData() async {
    setState(() {
      isLoading = false;
    });

    topicList = await ScopeDatabase.instance.readAllTopics();
    topicStringList.clear();
    for (int i = 0; i < topicList.length; i++) {
      topicStringList.add(topicList[i].gettopicName.toString());
    }

    setState(() {
      isLoading = true;
    });
  }

  Future<int> addDataInTopic(var topicName) async {
    Topic topic = new Topic(topicName);
    return await ScopeDatabase.instance.insertTopicData(topic);
  }
  /* Topic Data End */

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(Constants.padding)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child : isLoading ? contentBox(context) : CircularProgressIndicator()
    );
  }

  contentBox(context) {
    TextStyle? textStyle = Theme.of(context).textTheme.headline6;

    return Container(
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * 0.30,
      padding: EdgeInsets.all(Constants.padding),
      margin: EdgeInsets.all(Constants.padding),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.circular(Constants.padding),
          boxShadow: [
            BoxShadow(color: Colors.black, offset: Offset(0,5), blurRadius: 5)
          ]),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // Edit Topic
          Container(
              margin: EdgeInsets.only(top: 0.0),
              child: TypeAheadForm(
                  textStyle : textStyle,
                  textEditingController : suggestionTopicController,
                  labelText : StringClass.textTopic,
                  suggestionsFunction : getSuggestions,
                  refreshData : refreshTopicData,
                  addData : addDataInTopic,
                  list : topicStringList,
                  getSelectedSuggestion: getSelectedSuggestion)),
          Expanded(child:Container()),
          Center(
            child: Align(
                alignment: Alignment.bottomCenter,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).accentColor,
                      onPrimary: Colors.white),
                  child: Text(
                    StringClass.textSubmit,
                    textScaleFactor: 1.5,
                  ),
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).pop();
                      topicNameCallBack(selectedTopic);
                    });
                  },
                )
            ),
          )
        ],
      ),
    );
  }

  Future<List<String>> getSuggestions(String query, List<String> list) async {
    return await List.of(list).where((suggestion) {
      final suggestionLower = suggestion.toLowerCase();
      final queryLower = query.toLowerCase();
      return suggestionLower.contains(queryLower);
    }).toList();
  }
  
  void getSelectedSuggestion(String suggestion) {
    final topic = topicList.singleWhere((element) => element.gettopicName == suggestion);
    if (topic == null) {
      Fluttertoast.showToast(msg: StringClass.textPleaseSelectTopicNameAgain);
    } else {
      Fluttertoast.showToast(msg: topic.gettopicName.toString());
      selectedTopic = topic;
    }
  }
}

typedef TopicNameCallBack = void Function(Topic topic);