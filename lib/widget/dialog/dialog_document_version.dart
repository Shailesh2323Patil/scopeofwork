import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scope_of_work/utils/constants.dart';

class DialogDocumentVersion extends StatefulWidget {
  SelectEditDocument? selectEditDocument;
  SelectCopyDocument? selectCopyDocument;
  SelectDeleteDocument? selectDeleteDocument;

  DialogDocumentVersion({required this.selectEditDocument, required this.selectCopyDocument, required this.selectDeleteDocument});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DialogDocumentVersionState(selectEditDocument: selectEditDocument, selectCopyDocument: selectCopyDocument, selectDeleteDocument: selectDeleteDocument);
  }
}

class DialogDocumentVersionState extends State<DialogDocumentVersion> {
  SelectEditDocument? selectEditDocument;
  SelectCopyDocument? selectCopyDocument;
  SelectDeleteDocument? selectDeleteDocument;

  DialogDocumentVersionState({required this.selectEditDocument, required this.selectCopyDocument, required this.selectDeleteDocument});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(Constants.padding)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.80,
        height: MediaQuery.of(context).size.height * 0.30,
        padding: EdgeInsets.all(Constants.padding),
        margin: EdgeInsets.all(Constants.padding),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Constants.padding),
            boxShadow: [
              BoxShadow(
                  color: Colors.black, offset: Offset(0, 5), blurRadius: 5)
            ]),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                  selectEditDocument!();
                },
                child: Text(
                  "Edit Document",
                  style: TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.w600,
                      color: Colors.black),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    selectCopyDocument!();
                  },
                  child: Text(
                    "Copy Document",
                    style: TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.w600,
                        color: Colors.black),
                    textAlign: TextAlign.center,
                  )),
              SizedBox(
                height: 15,
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    selectDeleteDocument!();
                  },
                  child: Text(
                    "Delete Document",
                    style: TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.w600,
                        color: Colors.black),
                    textAlign: TextAlign.center,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

typedef SelectEditDocument = void Function();
typedef SelectCopyDocument = void Function();
typedef SelectDeleteDocument = void Function();