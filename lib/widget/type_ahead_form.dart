import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scope_of_work/utils/string_class.dart';

class TypeAheadForm extends StatefulWidget {
  TextStyle? textStyle;
  TextEditingController textEditingController;
  String labelText;
  Function suggestionsFunction;
  Function refreshData;
  Function addData;
  List<String> list;
  Function getSelectedSuggestion;

  TypeAheadForm({this.textStyle,
    required this.textEditingController,
    required this.labelText,
    required this.suggestionsFunction,
    required this.refreshData,
    required this.addData,
    required this.list,
    required this.getSelectedSuggestion});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return TypeAheadFormFieldState(
        textStyle,
        textEditingController,
        labelText,
        suggestionsFunction,
        refreshData,
        addData,
        list,
        getSelectedSuggestion);
  }
}

class TypeAheadFormFieldState extends State<TypeAheadForm> {
  TextStyle? textStyle;
  TextEditingController textEditingController;
  String labelText;
  Function suggestionsFunction;
  Function refreshData;
  Function addData;
  List<String> list;
  Function getSelectedSuggestion;

  TypeAheadFormFieldState(
      this.textStyle,
      this.textEditingController,
      this.labelText,
      this.suggestionsFunction,
      this.refreshData,
      this.addData,
      this.list,
      this.getSelectedSuggestion);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 0.0),
        child: textTypeAheadFormField(textStyle, textEditingController,
            labelText, suggestionsFunction, refreshData, addData, list,
            getSelectedSuggestion));
  }

  Widget textTypeAheadFormField(
      TextStyle? textStyle,
      TextEditingController textEditingController,
      String labelText,
      Function suggestionsFunction,
      Function refreshData,
      Function addData,
      List<String> list,
      Function getSelectedSuggestion) {

    return TypeAheadFormField<String?>(
        textFieldConfiguration: TextFieldConfiguration(
          controller: textEditingController,
          decoration: InputDecoration(
            labelText: labelText,
            labelStyle: textStyle,
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          ),
        ),
        suggestionsCallback: (pattern) async {
          return await suggestionsFunction(pattern, list);
        },
        itemBuilder: (context, String? suggestion) => ListTile(
              title: Text(suggestion!),
            ),
        noItemsFoundBuilder: (value) {
          return new GestureDetector(
            onTap: () {
              Future<int> id = addData(textEditingController.text);
              if (id != 0) {
                FocusScope.of(context).unfocus();
                new TextEditingController().clear();

                refreshTheMainData();
              } else {
                Fluttertoast.showToast(
                  msg: StringClass.textSomethingWentWrong,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                );
              }
            },
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Text(
                StringClass.textNoDataFound,
                style: TextStyle(
                    color: Colors.redAccent,
                    fontSize: 15,
                    fontWeight: FontWeight.normal),
              ),
            ),
          );
        },
        onSuggestionSelected: (String? suggestion) {
          textEditingController.text = suggestion!;
          getSelectedSuggestion(suggestion);
        });
  }

  Future<void> refreshTheMainData() async {
    await refreshData();
    /* Set the Data as Selected Data */
    getSelectedSuggestion(textEditingController.text);
  }
}
