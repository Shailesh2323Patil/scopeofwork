import 'dart:io';

import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:scope_of_work/model/document_details.dart';
import 'package:scope_of_work/model/pdf_client_info.dart';
import 'package:scope_of_work/page/pdf_generation/pdf_api.dart';
import 'package:scope_of_work/utils/string_class.dart';

class PdfInvoiceApi {
  static Future<File> generate(List<DocumentDetails> listDocumentDetails, List<ProjectClientInfo> projectClientInfoList) async {

    final image = (await rootBundle.load('assets/images/abcdesign.PNG')).buffer.asUint8List();
    final imageBullet = (await rootBundle.load('assets/images/icon_double_right.PNG')).buffer.asUint8List();
    final imageDot = (await rootBundle.load('assets/images/icon_filled_circle.PNG')).buffer.asUint8List();

    final pdf = Document();

    pdf.addPage(MultiPage(
        build: (context) =>
        [
          buildHeader(image),
          buildProjectInformation(projectClientInfoList[0]),
          buildBodyPart(listDocumentDetails,imageBullet,imageDot)
        ],
        footer: (context) => buildFooter()));

    return PdfApi.saveDocument(name: 'my_invoice.pdf', pdf: pdf);
  }

  static Widget buildHeader(var image) {
    return Center(
        child: ClipRRect(
          horizontalRadius: 0,
          verticalRadius: 0,
          child: Image(MemoryImage(image)),
        ));
  }

  static Widget buildFooter() =>
      Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Divider(),
          SizedBox(height: 2 * PdfPageFormat.mm),
          buildSimpleText(
              title: StringClass.textAddress,
              value: StringClass.textCompanyAddress),
          SizedBox(height: 1 * PdfPageFormat.mm),
          buildSimpleText(
              title: StringClass.textContact,
              value: StringClass.textCompanyContact),
        ],
      );

  static buildSimpleText({
    required String title,
    required String value,
  }) {
    final style = TextStyle(fontWeight: FontWeight.bold);

    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: pw.CrossAxisAlignment.end,
      children: [
        Text(title, style: style),
        SizedBox(width: 2 * PdfPageFormat.mm),
        Text(value),
      ],
    );
  }

  static buildProjectInformation(ProjectClientInfo projectClientInfo) {
    return Row(children: [
      Expanded(
          child: Column(
            children: [
              SizedBox(height: 2 * PdfPageFormat.mm,width: 20 * PdfPageFormat.mm),
              buildCustomerInfo(StringClass.textProjectName, projectClientInfo.projectName.toString() , MainAxisAlignment.start),
              buildCustomerInfo(StringClass.textContactPerson, projectClientInfo.clientName.toString(), MainAxisAlignment.start),
              buildCustomerInfo(StringClass.textMobileNumber, projectClientInfo.contactNumber.toString(), MainAxisAlignment.start),
            ],
          )),
      Expanded(
          child: Column(
            children: [
              buildCustomerInfo(
                  StringClass.textDate, projectClientInfo.date.toString(), MainAxisAlignment.end)
            ],
          )),
    ]);
  }

  static buildCustomerInfo(String key, String value, MainAxisAlignment mainAxisAlignment) {
    final styleKey = TextStyle(fontWeight: FontWeight.bold, fontSize: 13);
    final styleValue = TextStyle(fontSize: 13);

    return Container(
        child: Row(
          mainAxisAlignment: mainAxisAlignment,
          children: [
            Text(key, style: styleKey),
            Text(" : "),
            Text(value, style: styleValue),
          ],
        ));
  }

  static buildBodyPart(List<DocumentDetails> listDocumentDetails,var imageBullet,var imageDot) {
    return Container(child: Column(children: <Widget>[
        for (int i = 0; i < listDocumentDetails.length; i++)
          if (listDocumentDetails[i].topicId != 0)
            buildTopic(listDocumentDetails[i])
          else if (listDocumentDetails[i].titleId != 0)
            buildTitle(listDocumentDetails[i],imageBullet)
          else
            buildDescription(listDocumentDetails[i],imageDot)
      ]));
  }

  static buildTopic(DocumentDetails documentDetails) {
    return Container(
        alignment: Alignment.centerLeft,
        child: Column(
          children: [
            SizedBox(height: 5 * PdfPageFormat.mm),
            Text(documentDetails.topicName.toString(),
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: PdfColor.fromHex("#FF0000"),
                    fontSize: 16,
                    fontWeight: FontWeight.bold))
          ],
        ));
  }

  static buildTitle(DocumentDetails documentDetails,var imageBullet)  {
    return Container(
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 3 * PdfPageFormat.mm),
            Row(children: [
              SizedBox(height: 2 * PdfPageFormat.mm,width: 10 * PdfPageFormat.mm),
              Center(
                  child: ClipRRect(
                    horizontalRadius: 0,
                    verticalRadius: 0,
                    child: Container(
                      child: Image(MemoryImage(imageBullet)),
                      height: 15,
                      width: 15
                    ),
                  )),
              SizedBox(height: 2 * PdfPageFormat.mm,width: 3 * PdfPageFormat.mm),
              Text(documentDetails.titleName.toString(),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: PdfColor.fromHex("#000000"),
                      fontSize: 15,
                      fontWeight: FontWeight.bold))
            ])
          ],
        ));
  }

  static buildDescription(DocumentDetails documentDetails,var imageDot) {
    return Container(
        alignment: Alignment.centerLeft,
        child: Column(children: [
          SizedBox(height: 1 * PdfPageFormat.mm),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 2 * PdfPageFormat.mm,width: 20 * PdfPageFormat.mm),
              Center(
                  child: ClipRRect(
                    horizontalRadius: 0,
                    verticalRadius: 0,
                    child: Container(
                        height: 11,
                        width: 11,
                        child: Image(MemoryImage(imageDot))
                    ),
                  )),
              SizedBox(height: 2 * PdfPageFormat.mm,width: 3 * PdfPageFormat.mm),
              Text(documentDetails.descriptionName.toString(),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: PdfColor.fromHex("#000000"),
                      fontSize: 14))
            ],
          )
        ]));
  }
}
