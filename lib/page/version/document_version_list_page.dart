import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/document_type.dart';
import 'package:scope_of_work/model/document_version.dart';
import 'package:scope_of_work/utils/date_format.dart';
import 'package:scope_of_work/utils/string_class.dart';
import 'package:scope_of_work/page/version/document_version_list_widget.dart';

import 'document_version_add_edit_page.dart';

class DocumentVersionListPage extends StatefulWidget {

  int? projectId;

  DocumentVersionListPage({ required this.projectId});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DocumentVersionListState(projectId: projectId);
  }
}

class DocumentVersionListState extends State<DocumentVersionListPage> {
  int? projectId;

  DocumentVersionListState({ required this.projectId});

  bool isLoading = false;
  late List<DocumentVersion> documentVersionList;
  late List<DocumentType> documentTypeList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    refreshData();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // ScopeDatabase.instance.close();
    super.dispose();
  }

  Future refreshData() async {
    setState(() {
      isLoading = true;
    });

    this.documentVersionList = await ScopeDatabase.instance.readAllDocumentVersion(projectId!);
    this.documentTypeList = await ScopeDatabase.instance.readAllDocumentTypes();

    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        onWillPop: () async {
          return moveToLastScreen();
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text(StringClass.textDocumentVersionList),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                moveToLastScreen();
              },
            ),
          ),
          body: Center(
            child: isLoading
                ? CircularProgressIndicator()
                : documentVersionList.isEmpty
                  ? Text(StringClass.textPleaseAddDocumentVersion,
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black,fontSize: 24))
                : getDocumentVersionList()
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.deepPurple,
            child: Icon(Icons.add),
            onPressed: () {
              nextPage(new DocumentVersion("", projectId, documentTypeList[0].documentTypeId, Date_Format.format_2.format(DateTime.now()), Date_Format.format_2.format(DateTime.now())), "Add Document Version");
            },
          ),
        ));
  }

  Widget getDocumentVersionList() {
    var listItem = getListElement();

    var listView = ListView.builder(
        itemCount: listItem.length,
        itemBuilder: (context, index) {
          return DocumentVersionListWidget(documentVersion: listItem[index], context: context, refreshList: refreshData, position: index);
        });

    return listView;
  }

  List<DocumentVersion> getListElement()  {
    //var items = List<String>.generate(02, (index) => "Project $index");
    var items = documentVersionList;
    return items;
  }

  Future<bool> moveToLastScreen() async {
    Navigator.pop(context, true);
    return true;
  }

  void nextPage(DocumentVersion documentVersion,String appBarName) async {
      bool result = await Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => DocumentVersionAddAddEditPage(documentVersion,appBarName))
      );

      if(result == true) {
          refreshData();
      }
  }
}
