import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/document_version.dart';
import 'package:scope_of_work/model/project.dart';
import 'package:scope_of_work/utils/date_format.dart';
import 'package:scope_of_work/utils/string_class.dart';

class DocumentVersionAddAddEditPage extends StatefulWidget {
  final String appBarTitle;
  final DocumentVersion documentVersion;

  DocumentVersionAddAddEditPage(this.documentVersion, this.appBarTitle);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DocumentVersiontDetailsState(documentVersion, appBarTitle);
  }
}

class DocumentVersiontDetailsState extends State<DocumentVersionAddAddEditPage> {
  String appBarTitle;
  DocumentVersion globatDocumentVersion;

  DocumentVersiontDetailsState(this.globatDocumentVersion, this.appBarTitle);

  TextEditingController documentVersionNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    TextStyle? textStyle = Theme.of(context).textTheme.headline6;

    documentVersionNameController.text = globatDocumentVersion.getDocumentVersionName!;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(appBarTitle),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            moveToLastScreen();
          },
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Padding(
                padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
                child: TextField(
                  controller: documentVersionNameController,
                  style: textStyle,
                  onChanged: (value) {
                    globatDocumentVersion.documentVersionName = documentVersionNameController.text;
                  },
                  decoration: InputDecoration(
                      labelText: StringClass.textDocumentVersionName,
                      labelStyle: textStyle,
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
                )),

            // Third Element
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).accentColor,
                  onPrimary: Colors.white),
                  child: Text(
                    StringClass.textSubmit,
                    textScaleFactor: 1.5,
                  ),
                  onPressed: () {
                    setState(() {
                      addOrUpdateDocumentVersion();
                    });
              },
            )
          ],
        ),
      ),
    );
  }

  void addOrUpdateDocumentVersion() {
    if (globatDocumentVersion.documentVersionId == null) {
      Future<DocumentVersion> project = addDocumentVersion(globatDocumentVersion);
      project.then((value) => {moveToLastScreen()});
    } else {
      globatDocumentVersion.updatedDate = Date_Format.format_2.format(DateTime.now());
      Future<DocumentVersion> documentVersion = updateDocumentVersion(globatDocumentVersion);
      documentVersion.then((value) => {moveToLastScreenWithDocumentVersion(value)});
    }
  }

  Future<DocumentVersion> addDocumentVersion(DocumentVersion documentVersion) async {
    return await ScopeDatabase.instance.insertDocumentVersionData(documentVersion);
  }

  Future<DocumentVersion> updateDocumentVersion(DocumentVersion documentVersion) async {
    return await ScopeDatabase.instance.updateDocumentVersionData(documentVersion);
  }

  Future<bool> moveToLastScreen() async {
    Navigator.pop(context, true);
    return true;
  }

  Future<bool> moveToLastScreenWithDocumentVersion(DocumentVersion documentVersion) async {
    Navigator.pop(context, documentVersion);
    return true;
  }
}
