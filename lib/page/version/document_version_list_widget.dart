import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/document_details.dart';
import 'package:scope_of_work/model/document_version.dart';
import 'package:scope_of_work/page/document_details/document_details_page.dart';
import 'package:scope_of_work/page/version/document_version_add_edit_page.dart';
import 'package:scope_of_work/utils/string_class.dart';
import 'package:scope_of_work/widget/dialog/dialog_document_version.dart';

class DocumentVersionListWidget extends StatefulWidget {
  DocumentVersion documentVersion;
  BuildContext context;
  Function refreshList;
  int position;

  DocumentVersionListWidget({required this.documentVersion, required this.context, required this.refreshList,required this.position});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DocumentVersionListWidgetState(documentVersion: documentVersion, context: context, refreshList: refreshList,position: position);
  }
}

class DocumentVersionListWidgetState extends State<DocumentVersionListWidget> {
  DocumentVersion documentVersion;
  BuildContext context;
  int position;
  Function refreshList;

  DocumentVersionListWidgetState({required this.documentVersion, required this.context,required this.refreshList,required this.position});

  @override
  Widget build(BuildContext context) {

    String textDocumentName = StringClass.textDocumentName;
    String textCreateDate = StringClass.textCreateDate;
    String textUpdateDate = StringClass.textUpdateDate;

    // TODO: implement build
    return GestureDetector(
        onTap: () async {
          nextPage(documentVersion.getDocumentVersionId!);
        },
        child: Card(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 8,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(textDocumentName+" : "+documentVersion.getDocumentVersionName.toString(),
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 20,
                              fontWeight: FontWeight.bold)),
                      SizedBox(height: 4),
                      Text(textCreateDate+" : "+documentVersion.getCreatedDate.toString(),
                          style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                      SizedBox(height: 4),
                      Text(textUpdateDate+" : "+documentVersion.getUpdatedDate.toString(),
                          style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: IconButton(
                    icon: Icon(Icons.menu),
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return DialogDocumentVersion(
                                selectEditDocument: () {
                                  nextPageEditDocumentVersion(documentVersion,"Edit "+documentVersion.getDocumentVersionName.toString());
                                },
                                selectCopyDocument: () {
                                  copyTheDocumentVersion(documentVersion);
                                },
                                selectDeleteDocument: () {
                                  deleteDocumentVersion(documentVersion.documentVersionId);
                                });
                          });
                    },
                  ),
                )
              ],
            ),
          ),
        ));
  }

  void nextPage(int documentVersionId) async {
    bool result = await Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => DocumentDetailsPage(documentVersionId: documentVersionId))
    );
  }

  Future<DocumentVersion> addDocumentVersion(DocumentVersion documentVersion) async {
    return await ScopeDatabase.instance.insertDocumentVersionData(documentVersion);
  }

  Future<bool> moveToLastScreen() async {
    Navigator.pop(context, true);
    return true;
  }

  void nextPageEditDocumentVersion(DocumentVersion documentVersion,String appBarName) async {
    DocumentVersion resultDocumentVersion = await Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => DocumentVersionAddAddEditPage(documentVersion,appBarName))
    );
    refreshList();
  }

  void copyTheDocumentVersion(DocumentVersion documentVersion_1) async {

    /* Create the Document Version For Insertion */
    DocumentVersion forInsert = new DocumentVersion(documentVersion_1.documentVersionName!+" ${ StringClass.textCopy }", documentVersion_1.projectId, documentVersion_1.documentTypeId, documentVersion_1.createdDate, documentVersion_1.updatedDate);

    /* Insert New Document Version */
    DocumentVersion newDocumentVersion = await addDocumentVersion(forInsert);

    /* Fetch List Of Old DocumentDetails */
    List<DocumentDetails> documentDetailsList = await ScopeDatabase.instance.readAllDocumentDetailsForCopy(documentVersion_1.documentVersionId);

    await ScopeDatabase.instance.insertAllDocumentVersionAfterCopy(documentDetailsList, newDocumentVersion);

    refreshList();
  }

  void deleteDocumentVersion(int? documentVersionId) async {
    await ScopeDatabase.instance.deleteDocumentWithAllDocumentDetails(documentVersionId);
    refreshList();
  }
}

