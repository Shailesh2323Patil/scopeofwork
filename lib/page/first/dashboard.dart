import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:scope_of_work/model/client.dart';
import 'package:scope_of_work/page/client/client_add_edit.dart';
import 'package:scope_of_work/widget/note_card_widget.dart';
import 'package:scope_of_work/db/scope_database.dart';

class Dashboard extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DashboardPageState();
  }
}

class _DashboardPageState extends State<Dashboard> {
  String title = "Scope Of Work";
  bool isLoading = false;
  late List<Client> clientList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    refreshData();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // ScopeDatabase.instance.close();
    super.dispose();
  }

  Future refreshData() async {
    setState(() => isLoading = true);
    this.clientList = await ScopeDatabase.instance.readAllClients();
    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
     return Scaffold(
       appBar: AppBar(
         title: Text(
             title,
             style: TextStyle(fontSize: 24)
         ),
         actions: [Icon(Icons.search), SizedBox(width: 12)],
       ),
       body: Center(
         child: isLoading
              ? CircularProgressIndicator()
              : clientList.isEmpty
                ? Text(
                        "Please Add Your Client Information",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black, fontSize: 24))
             : buildNotes()
       ),
       floatingActionButton: FloatingActionButton(
         backgroundColor: Colors.deepPurple,
         child: Icon(Icons.add),
         onPressed: () {
            nextPage(new Client("",0,0,""),"Add Client");
         } ,
       ),
     );
  }

  Widget buildNotes() => StaggeredGridView.countBuilder(
    padding: EdgeInsets.all(8),
    itemCount: clientList.length,
    staggeredTileBuilder: (index) => StaggeredTile.fit(2),
    crossAxisCount: 4,
    mainAxisSpacing: 4,
    crossAxisSpacing: 4,
    itemBuilder: (context,index) {
      final client = clientList[index];
      return GestureDetector(
        onTap: () {
          nextPage(client,client.clientName!);
        },
        child: NoteCardWidget(client: client,index: index, buildContext: context),
      );
    },
  );

  void nextPage(Client client,String appBarName) async {
    bool result = await Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => ClientAddEditPage(client,appBarName))
    );

    if(result == true) {
      refreshData();
    }
  }
}












