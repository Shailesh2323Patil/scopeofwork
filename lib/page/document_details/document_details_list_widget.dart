import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scope_of_work/model/document_details.dart';
import 'package:scope_of_work/utils/string_class.dart';

class DocumentDetailsListWidget extends StatefulWidget {
  final DocumentDetails documentDetails;
  final BuildContext context;
  final List<DocumentDetails> listDocumentDetails;
  final int position;
  final Function deleteDocumentDetails;

  DocumentDetailsListWidget(
      {required this.documentDetails,
      required this.context,
      required this.listDocumentDetails,
      required this.position,
      required this.deleteDocumentDetails});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DocumentDetailsListWidgetState(
        documentDetails: documentDetails,
        context: context,
        listDocumentDetails: listDocumentDetails,
        position: position,
        deleteDocumentDetails: deleteDocumentDetails);
  }
}

class DocumentDetailsListWidgetState extends State<DocumentDetailsListWidget> {
  DocumentDetails documentDetails;
  BuildContext context;
  List<DocumentDetails> listDocumentDetails;
  int position;
  final Function deleteDocumentDetails;

  DocumentDetailsListWidgetState(
      {required this.documentDetails,
      required this.context,
      required this.listDocumentDetails,
      required this.position,
      required this.deleteDocumentDetails});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
        onTap: () async {},
        child: Card(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: [
                Expanded(flex: 8, child: selectWidget(documentDetails)),
                Expanded(
                  flex: 2,
                  child: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      deleteDocumentDetails(documentDetails);
                    },
                  ),
                )
              ],
            ),
          ),
        ));
  }

  Widget selectWidget(DocumentDetails documentDetails) {
    if (documentDetails.topicId != 0) {
      return Column(children: [
        SizedBox(
          width: double.infinity,
          child: Container(
            child: Text(StringClass.textTopic,
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.red.shade700,
                    fontSize: 20,
                    fontWeight: FontWeight.bold)),
          ),
        ),
        SizedBox(height: 4),
        SizedBox(
          width: double.infinity,
          child: Container(
            child: Text(documentDetails.topicName.toString(),
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.grey.shade700,
                    fontSize: 18,
                    fontWeight: FontWeight.bold)),
          ),
        )
      ]);
    } else if (documentDetails.titleId != 0) {
      return Column(
        children: [
          SizedBox(
            width: double.infinity,
            child: Container(
              child: Text(StringClass.textTitle,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Colors.green.shade700,
                      fontSize: 20,
                      fontWeight: FontWeight.bold)),
            ),
          ),
          SizedBox(height: 4),
          SizedBox(
            width: double.infinity,
            child: Container(
              child: Text(documentDetails.titleName.toString(),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Colors.grey.shade700,
                      fontSize: 18,
                      fontWeight: FontWeight.bold)),
            ),
          ),
        ],
      );
    } else {
      return Column(
        children: [
          SizedBox(
            width: double.infinity,
            child: Container(
              child: Text(StringClass.textDescription,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Colors.blue.shade700,
                      fontSize: 20,
                      fontWeight: FontWeight.bold)),
            ),
          ),
          SizedBox(height: 4),
          SizedBox(
            width: double.infinity,
            child: Container(
              child: Text(documentDetails.descriptionName.toString(),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Colors.grey.shade700,
                      fontSize: 18,
                      fontWeight: FontWeight.bold)),
            ),
          ),
        ],
      );
    }
  }

  Future<bool> moveToLastScreen() async {
    Navigator.pop(context, true);
    return true;
  }
}
