import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/description.dart';
import 'package:scope_of_work/model/document_details.dart';
import 'package:scope_of_work/model/pdf_client_info.dart';
import 'package:scope_of_work/model/title.dart';
import 'package:scope_of_work/model/topic.dart';
import 'package:scope_of_work/page/pdf_generation/pdf_invoice_api.dart';
import 'package:scope_of_work/page/pdf_generation/pdf_api.dart';
import 'package:scope_of_work/utils/string_class.dart';
import 'package:scope_of_work/widget/dialog/dialog_box_description_name.dart';
import 'package:scope_of_work/widget/dialog/dialog_box_select_type.dart';
import 'package:scope_of_work/widget/dialog/dialog_box_title_name.dart';
import 'package:scope_of_work/widget/dialog/dialog_box_topic_name.dart';
import 'document_details_list_widget.dart';

class DocumentDetailsPage extends StatefulWidget {
  int documentVersionId;

  DocumentDetailsPage({required this.documentVersionId});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DocumentDetailsState(documentVersionId: documentVersionId);
  }
}

class DocumentDetailsState extends State<DocumentDetailsPage> {
  int documentVersionId;
  DocumentDetailsState({required this.documentVersionId});
  bool isReLoad = false;
  bool isLoading = false;
  late List<DocumentDetails> documentDetailsList;
  late List<ProjectClientInfo> projectClientInfoList;

  Future refreshData() async {
    setState(() {
      isLoading = true;
    });

    this.documentDetailsList = await ScopeDatabase.instance.readAllDocumentDetails(documentVersionId);
    this.projectClientInfoList = await ScopeDatabase.instance.readProjectClientInfo(documentVersionId);

    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    refreshData();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return await moveToLastScreen();
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text(StringClass.textDocumentDetails),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                moveToLastScreen();
              },
            ),
            actions: [
              IconButton(
                  onPressed: () async {
                    if (isReLoad) {
                      await updateAllDocumentDetails(documentDetailsList);
                      Fluttertoast.showToast(msg: StringClass.textDataUpdatedSuccessfully);
                      isReLoad = false;
                    }
                    else {
                      final pdfFile = await PdfInvoiceApi.generate(documentDetailsList,projectClientInfoList);
                      PdfApi.openFile(pdfFile);
                    }
                  },
                  icon: Icon(Icons.print))
            ],
          ),
          body: Center(
              child: isLoading
                  ? CircularProgressIndicator()
                  : documentDetailsList.isEmpty
                      ? Text(StringClass.textPleaseAddYourData,
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black, fontSize: 24))
                      : getDocumentDetailsList()),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.deepPurple,
            child: Icon(Icons.add),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return DialogBoxSelectType(
                      selectTypeTopicName: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return DialogBoxTopic(
                                  topicNameCallBack: (Topic topic) {
                                if (topic.gettopicId != null) {
                                  DocumentDetails documentDetails = new DocumentDetails(documentVersionId, topic.gettopicId, 0, 0, 0);
                                  addOrUpdateDocumentDetails(documentDetails);
                                }
                              });
                            });
                      },
                      selectTypeTitleName: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return DialogBoxTitle(
                                titleNameCallBack: (Title_ title) {
                                  if (title.gettitleId != null) {
                                    DocumentDetails documentDetails =
                                        new DocumentDetails(documentVersionId,
                                            0, title.gettitleId, 0, 0);
                                    addOrUpdateDocumentDetails(documentDetails);
                                  }
                                },
                              );
                            });
                      },
                      selectTypeDescriptionName: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return DialogBoxDescription(
                                  descriptionNameCallBack:
                                      (Description description) {
                                if (description.getdescriptionId != null) {
                                  DocumentDetails documentDetails = new DocumentDetails(documentVersionId, 0, 0, description.getdescriptionId, 0);
                                  addOrUpdateDocumentDetails(documentDetails);
                                }
                              });
                            });
                      },
                    );
                  });
            },
          ),
        ));
  }

  Future<bool> moveToLastScreen() async {
    if (isReLoad) {
      await updateAllDocumentDetails(documentDetailsList);
      Fluttertoast.showToast(msg: StringClass.textDataUpdatedSuccessfully);

      isReLoad = false;

      return false;
    } else {
      Navigator.pop(context, true);
      return true;
    }
  }

  Widget getDocumentDetailsList() {
    var listItem = getListElement();

    var listView = ReorderableListView.builder(
        onReorder: (oldIndex, newIndex) => setState(() {
              final index = newIndex > oldIndex ? newIndex - 1 : newIndex;

              final docDetails = listItem.removeAt(oldIndex);
              listItem.insert(index, docDetails);

              isReLoad = true;
            }),
        itemCount: listItem.length,
        itemBuilder: (context, index) {
          DocumentDetails documentDetails = listItem[index];
          return Container(
              key: ValueKey(documentDetails),
              child: DocumentDetailsListWidget(
                  documentDetails: documentDetails,
                  context: context,
                  listDocumentDetails: documentDetailsList,
                  position: index,
                  deleteDocumentDetails: deleteDocumentDetails));
        });

    return listView;
  }

  List<DocumentDetails> getListElement() {
    var items = documentDetailsList;
    return items;
  }

  void addOrUpdateDocumentDetails(DocumentDetails documentDetails) async {
    if (documentDetails.docDetailsId == null) {
      DocumentDetails futureDocumentDetails = await addDocumentDetails(documentDetails);
      await refreshData();
    }
  }

  Future<DocumentDetails> addDocumentDetails(DocumentDetails documentDetails) async {
    return await ScopeDatabase.instance.insertDocumentDetails(documentDetails);
  }

  Future<DocumentDetails> updateDocumentDetails(DocumentDetails documentDetails) async {
    return await ScopeDatabase.instance.updateDocumentDetails(documentDetails);
  }

  Future updateAllDocumentDetails(List<DocumentDetails> listDocumentDetails) async {
    return await ScopeDatabase.instance.updateAllDocumentDetails(listDocumentDetails);
  }

  void deleteDocumentDetails(DocumentDetails documentDetails) async {
    var value = await ScopeDatabase.instance
        .deleteDocumentDetails(documentDetails.docDetailsId);
    if (value == true) {
      refreshData();
    }
  }
}
