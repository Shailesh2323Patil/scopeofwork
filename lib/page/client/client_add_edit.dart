import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/client.dart';

class ClientAddEditPage extends StatefulWidget {
  final String appBarTitle;
  final Client client;

  ClientAddEditPage(this.client, this.appBarTitle);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ClientDetailsState(client, appBarTitle);
  }
}

class ClientDetailsState extends State<ClientAddEditPage> {
  String appBarTitle;
  Client globalClient;

  ClientDetailsState(this.globalClient, this.appBarTitle);

  TextEditingController clientNameController = TextEditingController();
  TextEditingController contactNumberController = TextEditingController();
  TextEditingController alternateNumberController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    TextStyle? textStyle = Theme.of(context).textTheme.headline6;

    clientNameController.text = globalClient.getClientName!;

    String contactNumber = globalClient.getContactNumber.toString();
    if (contactNumber != "0") {
      contactNumberController.text = contactNumber;
    }

    String alternateNumber = globalClient.getAlternateNumber.toString();
    if (alternateNumber != "0") {
      alternateNumberController.text = alternateNumber;
    }

    emailController.text = globalClient.getEmailId!;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(appBarTitle),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              moveToLastScreen();
            }),
      ),
      body: Container(
        child: Column(
          children: [
            // First Element
            Padding(
              padding: EdgeInsets.only(top: 20.0,left: 10.0,right: 10.0),
              child: TextField(
                controller: clientNameController,
                style: textStyle,
                onChanged: (value) {
                  globalClient.clientName = clientNameController.text;
                },
                decoration: InputDecoration(
                    labelText: 'Client Name',
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0))),
              ),
            ),

            // Second Element
            Padding(
                padding: EdgeInsets.only(top: 10.0,left: 10.0,right: 10.0),
                child: TextField(
                  controller: contactNumberController,
                  style: textStyle,
                  onChanged: (value) {
                    globalClient.contactNumber =
                        int.parse(contactNumberController.text);
                  },
                  decoration: InputDecoration(
                      labelText: 'Contact Number',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0))),
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                )),

            // Third Element
            Padding(
                padding: EdgeInsets.only(top: 10.0,left: 10.0,right: 10.0),
                child: TextField(
                  controller: alternateNumberController,
                  style: textStyle,
                  onChanged: (value) {
                    globalClient.alternateNumber =
                        int.parse(alternateNumberController.text);
                  },
                  decoration: InputDecoration(
                      labelText: "Alternet Contact Number",
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0))),
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                )),

            // Forth Element
            Padding(
                padding: EdgeInsets.only(top: 10.0,left: 10.0,right: 10.0),
                child: TextField(
                  controller: emailController,
                  style: textStyle,
                  onChanged: (value) {
                    globalClient.emailId = emailController.text;
                  },
                  decoration: InputDecoration(
                      labelText: "Email Id",
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0))),
                )),

            // Fifth Element
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).accentColor,
                  onPrimary: Colors.white),
                  child: Text(
                    'Submit',
                    textScaleFactor: 1.5,
                  ),
                  onPressed: () {
                    setState(() {
                      addOrUpdateNote();
                    });
                  },)
          ],
        ),
      ),
    );
  }

  void addOrUpdateNote() {
    if (globalClient.clientId == null) {
      Future<Client> client = addClient(globalClient);
      client.then((value) => {moveToLastScreen()});
    } else {
      Future clientId = updateClient(globalClient);
      clientId.then((value) => {moveToLastScreen()});
    }
  }

  Future<Client> addClient(Client client) async {
    return await ScopeDatabase.instance.insertData(client);
  }

  Future<int> updateClient(Client client) async {
    return await ScopeDatabase.instance.updateData(client);
  }

  Future<bool> moveToLastScreen() async {
    Navigator.pop(context, true);
    return true;
  }
}
