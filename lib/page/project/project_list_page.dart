import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/project.dart';
import 'package:scope_of_work/page/project/project_add_edit_page.dart';
import 'package:scope_of_work/page/project/project_list_widget.dart';

class ProjectListPage extends StatefulWidget {

  int? clientId;

  ProjectListPage({ required this.clientId});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProjectListState(clientId: clientId);
  }
}

class ProjectListState extends State<ProjectListPage> {

  int? clientId;

  ProjectListState({ required this.clientId});

  bool isLoading = false;
  late List<Project> projectList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    refreshData();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // ScopeDatabase.instance.close();
    super.dispose();
  }

  Future refreshData() async {
    setState(() {
      isLoading = true;
    });

    this.projectList = await ScopeDatabase.instance.readAllProject(clientId!);

    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        onWillPop: () async {
          return moveToLastScreen();
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text("Project List"),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                moveToLastScreen();
              },
            ),
          ),
          body: Center(
            child: isLoading
                ? CircularProgressIndicator() : projectList.isEmpty
                ? Text("Please Add Yor Project",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black,fontSize: 24))
                : getProjectList()
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.deepPurple,
            child: Icon(Icons.add),
            onPressed: () {
              nextPage(new Project(clientId,"",""), "Add Project");
            },
          ),
        ));
  }

  Widget getProjectList() {
    var listItem = getListElement();

    var listView = ListView.builder(
        itemCount: listItem.length,
        itemBuilder: (context, index) {
          return ProjectListWidget(project: listItem[index], context: context,listProject: listItem,position: index);
        });

    return listView;
  }

  List<Project> getListElement()  {
    //var items = List<String>.generate(02, (index) => "Project $index");
    var items = projectList;
    return items;
  }

  Future<bool> moveToLastScreen() async {
    Navigator.pop(context, true);
    return true;
  }

  void nextPage(Project project,String appBarName) async {
      bool result = await Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => ProjectAddEditPage(project,appBarName))
      );

      if(result == true) {
          refreshData();
      }
  }
}
