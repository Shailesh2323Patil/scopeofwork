import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scope_of_work/db/scope_database.dart';
import 'package:scope_of_work/model/project.dart';
import 'package:scope_of_work/utils/date_format.dart';

class ProjectAddEditPage extends StatefulWidget {
  final String appBarTitle;
  final Project project;

  ProjectAddEditPage(this.project, this.appBarTitle);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProjectDetailsState(project, appBarTitle);
  }
}

class ProjectDetailsState extends State<ProjectAddEditPage> {
  String appBarTitle;
  Project globalProject;

  ProjectDetailsState(this.globalProject, this.appBarTitle);

  TextEditingController? projectNameController;
  TextEditingController? dateController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    projectNameController = TextEditingController();
    dateController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    projectNameController?.dispose();
    dateController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    TextStyle? textStyle = Theme.of(context).textTheme.headline6;

    projectNameController?.text = globalProject.getProjectName!;
    dateController?.text = globalProject.getDate!;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(appBarTitle),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            moveToLastScreen();
          },
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Padding(
                padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
                child: TextField(
                  controller: projectNameController,
                  style: textStyle,
                  onChanged: (value) {
                    globalProject.projectName = projectNameController?.text;
                  },
                  decoration: InputDecoration(
                      labelText: "Project Name",
                      labelStyle: textStyle,
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
                )),

            // Second Element
            Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: TextField(
                  readOnly: true,
                  enableInteractiveSelection: true,
                  onTap: () async {
                    final DateTime? pickedDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2025),
                    );
                    if (pickedDate != null) {
                      dateController?.text = Date_Format.format.format(pickedDate);
                      globalProject.date = Date_Format.format.format(pickedDate);
                    }
                  },
                  controller: dateController,
                  style: textStyle,
                  onChanged: (value) {
                    globalProject.date = dateController?.text;
                  },
                  decoration: InputDecoration(
                      labelText: "Date",
                      labelStyle: textStyle,
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
                )),

            // Third Element
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).accentColor,
                  onPrimary: Colors.white),
                  child: Text(
                    'Submit',
                    textScaleFactor: 1.5,
                  ),
                  onPressed: () {
                    addOrUpdateProject();
                })
          ],
        ),
      ),
    );
  }

  void addOrUpdateProject() {
    if (globalProject.projectId == null) {
      Future<Project> project = addProject(globalProject);
      project.then((value) => {moveToLastScreen()});
    } else {
      Future<Project> project = updateProject(globalProject);
      project.then((value) => {moveToLastScreenWithProject(value)});
    }
  }

  Future<Project> addProject(Project project) async {
    return await ScopeDatabase.instance.insertProjectData(project);
  }

  Future<Project> updateProject(Project project) async {
    return await ScopeDatabase.instance.updateProjectData(project);
  }

  Future<bool> moveToLastScreen() async {
    Navigator.pop(context, true);
    return true;
  }

  Future<bool> moveToLastScreenWithProject(Project project) async {
    Navigator.pop(context, project);
    return true;
  }
}





















































































































































































































































































































































































































