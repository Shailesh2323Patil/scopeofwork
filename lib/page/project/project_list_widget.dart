import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scope_of_work/model/project.dart';
import 'package:scope_of_work/page/project/project_add_edit_page.dart';
import 'package:scope_of_work/page/version/document_version_list_page.dart';
import 'package:scope_of_work/utils/string_class.dart';

class ProjectListWidget extends StatefulWidget {
  final Project project;
  final BuildContext context;
  final List<Project> listProject;
  final int position;

  ProjectListWidget(
      {required this.project,
      required this.context,
      required this.listProject,
      required this.position});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProjectListWidgetState(
        project: project,
        context: context,
        listProject: listProject,
        position: position);
  }
}

class ProjectListWidgetState extends State<ProjectListWidget> {
  Project project;
  BuildContext context;
  List<Project> listProject;
  int position;

  ProjectListWidgetState(
      {required this.project,
      required this.context,
      required this.listProject,
      required this.position});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
        onTap: () async {
          nextPage(project.projectId);
        },
        child: Card(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  flex: 8,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(project.getProjectName!,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 20,
                              fontWeight: FontWeight.bold)),
                      SizedBox(height: 4),
                      Text(project.getDate!,
                          style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () {
                      naxtPageEditProject(project);
                    },
                  ),
                )
              ],
            ),
          ),
        ));
  }

  void nextPage(int? projectId) async {
    bool result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => DocumentVersionListPage(projectId: projectId)));
  }

  void naxtPageEditProject(Project projectSend) async {
    Project resultProject = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>
            ProjectAddEditPage(projectSend, StringClass.textEditProject)));
    setState(() {
      listProject[position] = resultProject;
    });
  }

  Future<bool> moveToLastScreen() async {
    Navigator.pop(context, true);
    return true;
  }
}
