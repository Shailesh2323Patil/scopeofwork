import 'package:intl/intl.dart';


class Date_Format {
  static var format = new DateFormat("dd-MM-yyyy");
  static var format_2 = new DateFormat("dd-MM-yyyy HH:mm");
}