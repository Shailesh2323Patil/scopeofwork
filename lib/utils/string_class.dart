class StringClass {
  static const String textCompanyAddress = "405, Akruti Arcade, J P Road, Andheri (W), Mumbai - 400 053";
  static const String textCompanyContact = "+91 8433303188  |  info@abcdesigns.in  |  www.abcdesigns.in";
  static const String textNoDataFound = "No Data Found, Add Data in Master";
  static const String textDataAddedSuccessfully = "Data Added Successfully";
  static const String textSomethingWentWrong = "Something Went Wrong";
  static const String textTopic = "Topic";
  static const String textTitle = "Title";
  static const String textDescription = "Description";
  static const String textAddress = "Address";
  static const String textContact = "Contact";
  static const String textDocumentVersionList = "Document Version List";
  static const String textPleaseAddDocumentVersion = "Please Add Document Version";
  static const String textDocumentVersionName = "Document Version Name";
  static const String textSubmit = "Submit";
  static const String textEditProject = "Edit Project";
  static const String textDocumentDetails = "Document Details";
  static const String textPleaseAddYourData = "Please Add Your Data";
  static const String textPleaseSelectTopicNameAgain = "Please Select Topic Name Again";
  static const String textDataUpdatedSuccessfully = "All Data Updated Successfully.";
  static const String textCopy = "Copy";
  static const String textDocumentName = "Document Name";
  static const String textCreateDate = "Create Date";
  static const String textUpdateDate = "Update Date";
  static const String textLoremIpsum = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

  static const String textContactPerson = "Contact Person";
  static const String textMobileNumber = "Mobile Number";
  static const String textProjectName = "Project / Name";
  static const String textShortBrief = "Short Brief";
  static const String textDate = "Date";
}